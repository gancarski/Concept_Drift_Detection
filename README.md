# Concept_Drift_Detection : Supervised and Unsupervised Concept Drift Detection Methods

*by Mario Gancarski, 2022.*

First work : re-implementing two existent algorithms in order to understand how they work out.

- 1) DDM
- 2) D3


Work on the dynamic windowing and on handling multiple changes in one window :

- 1) Klinkenberg 1999 Adaptive Information Filtering: Learning in the Presence of Concept Drifts
Here the idea is to the window size with 3 possibilities : 
    - abrupt drifts : reduce the window size to the minimum, because the distribution is likely to change a lot in the next samples
    - gradual drifts : reduce a little bit the window size
    - no drift : increase a little bit the window size 
These changes are limited to a predefined minimum bound and maximum bound

---

**Sample run :**

```
from DDM import DDM
from D3 import D3
from datasets import import_dataset
import matplotlib.pyplot as plt
import numpy as np

X, y = import_dataset('datasets/elec.csv')
window_size = 100
rho = 0.1
threshold = 0.60
dim = len(X[0])
newX = np.array(X)
newY = np.array(y)
d3 = D3(newX, newY, window_size, rho, threshold, dim)
ddm = DDM(X, y)

error_rate_list, accuracy = d3.run_all_steps()
print(accuracy)
plt.plot(error_rate_list, color = 'blue')
plt.show()

```


- Dynamic windows

- Multiple changes in one window
